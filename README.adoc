= Viaje por carretera

Estamos organizando un viaje por carretera desde Sevilla hasta Barcelona ida y vuelta. Tenemos un total de 15 días para realizarlo y queremos aprovechar al máximo el tiempo de viaje. Para ello estamos organizando las paradas y los días que estaremos en cada lugar. 

Proyecto del ejercicio 5 para la asignatura *Planificación y Gestión de Proyectos
Informáticos*.

Los archivos de este proyecto están en formato https://asciidoctor.org/docs/what-is-asciidoc[Asciidoc] y pueden ser procesados mediante el paquete https://asciidoctor.org[Asciidoctor]. *Asciidoc* es un formato de marcas ligeros similar a *Markdown* pero mucho más potente y estandarizado.

== Cómo usar

Si tu también estas pensando en irte de viaje, podría servirte de guía.

=== Editar documentos

*Asciidoc* es texto plano. Puede editarse con cualquier editor de texto. https://atom.io/[Atom] es un editor de texto avanzado con excelente soporte para Asciidoc a través del paquete https://github.com/asciidoctor/atom-asciidoc-assistant[asciidoc-assistant].

Tienes toda la información sobre el formato en la página del proyecto https://asciidoctor.org[Asciidoctor].

=== Procesar archivos

Para procesar los archivos y generar un documento en HTML necesitas instalar el programa *asciidoctor*. Las instrucciones de instalación están en la página principal del proyecto https://asciidoctor.org[Asciidoctor].

Para generar un documento *index.html* único con todo el contenido basta procesar el archivo *index.adoc*:

    $ asciidoctor index.adoc

Si instalalas https://asciidoctor.org/docs/asciidoctor-pdf/[asciidoctor-pdf] podrás generar un documento en formato PDF:

    $ asciidoctor-pdf -o viajeros-pgpi-Ej5.pdf index.adoc

== Autores
//Ordenar alfabéticamente

* Bazán Romero, José Antonio

* Brenes Reyes, Javier

* Campos Ramos, Adrián

* Olabarrieta Eduardo, Iraia

* Posada López, María José

== Licencia

* *GPL*  (Licencia Pública General de GNU)

Este repositorio es documentación libre: puede redistribuirlo y / o modificarlo bajo los términos de la Licencia Pública General GNU publicada por _Free Software Foundation_, ya sea la versión 3 de la Licencia, o (a su elección) cualquier versión posterior.

Este programa se distribuye con la esperanza de que sea útil, pero *SIN NINGUNA GARANTÍA*; sin siquiera la garantía implícita de *COMERCIABILIDAD o APTITUD PARA UN PROPÓSITO EN PARTICULAR*. Ver el Licencia pública general de GNU para más detalles.

Debería haber recibido una copia de la Licencia Pública General de GNU junto con este programa. Si no, vea https://www.gnu.org/licenses/[el siguiente enlace] 
